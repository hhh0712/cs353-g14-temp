import firebase from "firebase";
var firebaseConfig = {
    apiKey: "AIzaSyCBNcqdY9n2MeH-tN9sTr9l1VwAjtc9HxM",
    authDomain: "cs353-g14.firebaseapp.com",
    projectId: "cs353-g14",
    storageBucket: "cs353-g14.appspot.com",
    messagingSenderId: "316237375748",
    appId: "1:316237375748:web:d221a16f8f0c18976d0851",
    measurementId: "G-W1VB2378W2"
  };
// Initialize Firebase
const fire = firebase.initializeApp(firebaseConfig);

export default fire;